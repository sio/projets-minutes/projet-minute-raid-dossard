GRANT USAGE ON SCHEMA "raid_dossard" TO "etudiants-slam";
GRANT SELECT ON TABLE "raid_dossard"."activite" TO "etudiants-slam";
GRANT SELECT ON TABLE "raid_dossard"."activite_raid" TO "etudiants-slam";
GRANT SELECT ON TABLE "raid_dossard"."coureur" TO "etudiants-slam";
GRANT SELECT ON TABLE "raid_dossard"."equipe" TO "etudiants-slam";
GRANT SELECT ON TABLE "raid_dossard"."integrer_equipe" TO "etudiants-slam";
GRANT SELECT ON TABLE "raid_dossard"."raid" TO "etudiants-slam";
